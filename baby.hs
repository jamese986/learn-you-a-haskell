-- Baby's first functions

-- Double
doubleMe x = x + x

doubleUs x y = doubleMe x + doubleMe y

doubleSmallNumber' x = (if x > 100 then x else x*2) + 1

-- Functions Can't start with a capital 
-- Functions which dont take params are usually called definitions
conanO'Brien = "It's a-me, Conan O'Brien!"

-- Predicate / Filtering
boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]   

removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']]   
